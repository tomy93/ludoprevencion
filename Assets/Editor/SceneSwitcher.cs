using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEditor.SceneManagement;

public class SceneSwitcher : MonoBehaviour {
    [MenuItem("OpenScene/Loading")]
    static void Loading()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/_Scenes/"+ScenesNamesLibrary.LOADING+".unity");
    }
    [MenuItem("OpenScene/HomeMenu")]
    static void HomeMenu()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/_Scenes/"+ScenesNamesLibrary.HOME_MENU+".unity");
    }
    [MenuItem("OpenScene/Highscores")]
    static void Highscores()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/_Scenes/" + ScenesNamesLibrary.HIGHSCORES + ".unity");
    }

    [MenuItem("OpenScene/Game/Domino")]
    static void Domino()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/_Scenes/" + ScenesNamesLibrary.DOMINO + ".unity");
    }

    [MenuItem("OpenScene/Game/Solitary")]
    static void Solitary()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/_Scenes/" + ScenesNamesLibrary.SOLITARY + ".unity");
    }

    [MenuItem("OpenScene/Game/AccidentCase")]
    static void AccidentCase()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/_Scenes/" + ScenesNamesLibrary.ACCIDENT_CASE + ".unity");
    }
}

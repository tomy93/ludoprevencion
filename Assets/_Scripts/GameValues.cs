using System.Collections.Generic;
using UnityEngine;

public class GameValues {
    public static int FinalScore { get; internal set; }

    public static User CurrentUser;
}

public class DominosValues {
	public static bool FirstTimePlaying = true;
	public static bool GameEnded = false;
    public static DominoPositionVector[] aDomPosVec;   
    public static int CurrentDominoIdDragged;
    public static DominoEntity LastDominoDragged;   
    
    public static List<GameObject> lDomTable;  
    public static List<GameObject> lDomBoard;

    public const float DOMINO_SIZE_FACTOR = 105;//debe ser seteado manualmente tambi�n, es la distancia entre los dos Sides de un domino
    public const int TOTAL_CURRENT_INCORRECT_MOVES = 3;//total de movimientos incorrectos para que te salte el mensaje de que est�s jugando mal
    public const int TOTAL_DOMINOS_AMOUNT = 10; //valor necesario para determinar cuando se gana el juego, debe ser por defecto 10
    
    private static int currentIncorrectMoves = 0;
    private static int globalIncorrectMoves = 0;
    private static int finalScore = 0;
    private static int correctMoves = 0;

    public static int CurrentIncorrectMoves { get {return currentIncorrectMoves;   } protected set { currentIncorrectMoves = value;} }
    public static int GlobalIncorrectMoves { get {return globalIncorrectMoves;   } protected set { globalIncorrectMoves = value;} }
    public static int FinalScore { get {return finalScore;   } protected set { finalScore = value;} }
    public static int CorrectMoves { get {return correctMoves;   } protected set { correctMoves = value;} }
    
}

public class SolitaryValues {
	public static bool GameEnded = false;
    public const int TOTAL_CURRENT_INCORRECT_MOVES = 3;//total de movimientos incorrectos para que te salte el mensaje de que est�s jugando mal
    public const int TOTAL_CARDS_AMOUNT = 25; //valor necesario para determinar cuando se gana el juego, tiene que ser 25

    private static int currentIncorrectMoves = 0;
    private static int globalIncorrectMoves = 0;
    private static int finalScore = 0;
    private static int correctMoves = 0;

    public static int CurrentIncorrectMoves { get {return currentIncorrectMoves;   } protected set { currentIncorrectMoves = value;} }
    public static int GlobalIncorrectMoves { get {return globalIncorrectMoves;   } protected set { globalIncorrectMoves = value;} }
    public static int FinalScore { get {return finalScore;   } protected set { finalScore = value;} }
    public static int CorrectMoves { get {return correctMoves;   } protected set { correctMoves = value;} }
}

public class CaseGameValues{
	public static bool GameEnded = false;  
    public const int TOTAL_TIME = 100; // tiempo por default tiene que ser 50
    public const int MAX_AMOUNT_VALIDATIONS = 3;
    
    private static int currentTimeLeft = 0;
	public static int CurrentTimeLeft { get {return currentTimeLeft;   } protected set { currentTimeLeft = value;} }
    private static int currentValidations = 0;
	public static int CurrentValidations { get {return currentValidations;   } protected set { currentValidations = value;} }
	private static int correctMoves = 0;
	public static int CorrectMoves { get {return correctMoves;   } protected set { correctMoves = value;} }
	private static int incorrectMoves = 0;
	public static int IncorrectMoves { get {return incorrectMoves;   } protected set { incorrectMoves = value;} }
    private static int finalScore = 0;
    public static int FinalScore { get {return finalScore;   } protected set { finalScore = value;} }


}

public class User {
    public int ID;
    public string Name;
    public string Country;
    public int Score;

    public User(int _id, string _name,string _country, int _score) {
        ID = _id;
        Name = _name;
        Country = _country;
        Score = _score;
    }
}

public class DominoPositionVector {
    public int SlotID;
    public int DominoID;
}

public enum EnumDominoLocations {
    None,
    Table,
    Board
}

public enum EnumDominoOrientation {
    None,
    Portrait,
    Landscape
}

public enum EnumCardType {
    None,
    Type0,
    Type1,
    Type2,
    Type3,
    Type4
}
public enum EnumCaseType {
	None,
	Type0,
	Type1,
	Type2,
	Type3,
}

public enum EnumGames{
	None,
	Domino,
	Solitary,
	CaseGame
}

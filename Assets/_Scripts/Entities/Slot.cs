using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Slot : MonoBehaviour, IDropHandler {
    public int SlotID;
    public EnumDominoLocations Location;
    public EnumDominoOrientation Orientation;
    private Image img;
    private Color defaultColor;
    //public int CurrentMatchOfAnotherSlot;

    void Awake() {
        img = GetComponent<Image>();
        defaultColor = img.color;
    }
    public GameObject Item {
        get {
            if(transform.childCount>0) {
                return transform.GetChild(0).gameObject;
            }
            return null;
        }
    }

    public void OnDrop(PointerEventData eventData) {
        if(!Item) {
            HandleDrag.itemBeingDragged.transform.SetParent(transform);
            ExecuteEvents.ExecuteHierarchy<IHaschanged>(gameObject,null,(x,y) => x.HasChanged());            
            OnChange();

            if(Location == EnumDominoLocations.Table) {
                DominosActions.OnDropInTable();
            }
        }
    }

    internal void Hide() {
        img.color = new Color(0,0,0,0);
    }
    internal void Show() {
       img.color = defaultColor;
    }
    public void SwapOrientationToPortrait() {        
        transform.localEulerAngles = Vector2.zero;
        if(Orientation == EnumDominoOrientation.Landscape) {
            Orientation = EnumDominoOrientation.Portrait;
        }
    }
    public void SwapOrientation() {
        if(Orientation == EnumDominoOrientation.Landscape) {
            Orientation = EnumDominoOrientation.Portrait;
            if(transform.childCount>0) {
                transform.GetChild(0).GetComponent<DominoEntity>().Orientation = EnumDominoOrientation.Portrait;
            }
        }else if(Orientation == EnumDominoOrientation.Portrait) {
            Orientation = EnumDominoOrientation.Landscape;
            if(transform.childCount>0) {
                transform.GetChild(0).GetComponent<DominoEntity>().Orientation = EnumDominoOrientation.Landscape;
            }
        }
    }

    public virtual void OnChange() {
        
    }
}

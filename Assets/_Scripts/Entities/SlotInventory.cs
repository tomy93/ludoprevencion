﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SlotInventory : MonoBehaviour, IHaschanged {
    [SerializeField] private Transform slots;

    void Start () {
		HasChanged();
	}

    private void tableOrderChanged() {
        
    }
    
    //currently not in use
    public void HasChanged() {
        return;
        /*
        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        builder.Append(" - ");
        foreach(Transform slotTransform in slots) {
            GameObject item = slotTransform.GetComponent<Slot>().Item;
            if(item) {
                builder.Append(item.name);
                builder.Append( " - ");
            }
        }*/
    }
}

public interface IHaschanged : IEventSystemHandler {
    void HasChanged();
}

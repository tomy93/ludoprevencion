using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardEntity : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
    public int id;
    public EnumCardType cardType;
    public bool isLocked = false;

    public static CardEntity itemBeingDragged;
    public Vector2 startPosition;
    private Transform startParent;
    private CanvasGroup cG;

    void Awake() {        
        cG = GetComponent<CanvasGroup>();
       
    }
    void Start () {
		
	}
	
	void Update () {
		
	}
    
    public void OnBeginDrag(PointerEventData eventData) {
        if(isLocked)return;
        itemBeingDragged = this;
        startPosition = GetComponent<RectTransform>().localPosition;
        startParent = transform.parent;
        cG.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData) {
        if(isLocked)return;
		//when canvas is screen space overlay:
        //transform.position = Input.mousePosition;

		//when canvas is screen space camera:
		Vector3 screenPoint = Input.mousePosition;
		screenPoint.z = 100f; //distance of the plane from the camera
		transform.position = Camera.main.ScreenToWorldPoint(screenPoint);
    }

    //later than OnDrop of IDropHandler
    public void OnEndDrag(PointerEventData eventData) {
        if(isLocked)return;

        itemBeingDragged = null;
        cG.blocksRaycasts = true;
        if(transform.parent == startParent) {
           // transform.position = startPosition;
           GetComponent<RectTransform>().localPosition = startPosition;

        }else {
           //transform.position = transform.parent.position;            
          // GetComponent<RectTransform>().localPosition = startPosition;

        }
    }
}

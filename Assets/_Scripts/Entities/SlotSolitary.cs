using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SlotSolitary : MonoBehaviour, IDropHandler {
    public int id;
    public EnumCardType cardType;
    public float distanceBetweenElements = 50;
	[SerializeField] private HelpMessageDisplay helpMessage;
    
    //earlier than OnEndDrag of IEndDragHandler
    public void OnDrop(PointerEventData eventData) {
        if(CardEntity.itemBeingDragged.cardType != cardType) {
            CardEntity.itemBeingDragged.GetComponent<RectTransform>().localPosition = CardEntity.itemBeingDragged.startPosition;
			bool x;
			SolitaryValuesConfig.DoneIncorrectMove(out x);
			if (x == true) {
				helpMessage.DisplayMessage ();
			}
            return;
        }
        
        SolitaryValuesConfig.DoneCorrectMove();
        CardEntity.itemBeingDragged.isLocked = true;
        CardEntity.itemBeingDragged.transform.SetParent(transform);

        CardEntity.itemBeingDragged.transform.localPosition = new Vector2(0, newItemPositionY());
    }

    private float newItemPositionY() {
        return -(transform.childCount-1)*distanceBetweenElements;
    }
}

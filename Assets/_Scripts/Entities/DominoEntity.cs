using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DominoEntity : MonoBehaviour {
    public int dominoID;
    [SerializeField]    private DominoSide dominoSide_A;
    [SerializeField]    private DominoSide dominoSide_B;
    [HideInInspector]public bool[] aMatchInPosition;
    public EnumDominoOrientation Orientation;
    [HideInInspector]public Transform TableParent;
    public bool Locked = false;
    private float distanceFromSides;

    void Start () {
		aMatchInPosition = new bool[6];
        for(int i = 0; i < aMatchInPosition.Length; i++) {
            aMatchInPosition[i] = false;
        }
        distanceFromSides = (float)Math.Ceiling(Math.Floor(Vector2.Distance(dominoSide_A.trans.position, dominoSide_B.trans.position)*100f) /100f);
        //Debug.Log(distanceFromSides);
	}
	
	void Update () {
	}

    public bool MatchWith(DominoEntity domino) {
       /* Debug.Log("a + dominoB "+Math.Ceiling(Vector2.Distance(dominoSide_A.trans.position,domino.dominoSide_B.trans.position)));
        Debug.Log("a + dominoA "+Math.Ceiling(Vector2.Distance(dominoSide_A.trans.position,domino.dominoSide_A.trans.position)));
        Debug.Log("b + dominoB "+Math.Ceiling(Vector2.Distance(dominoSide_B.trans.position,domino.dominoSide_B.trans.position)));
        Debug.Log("b + dominoA "+Math.Ceiling(Vector2.Distance(dominoSide_B.trans.position,domino.dominoSide_A.trans.position)));*/

            if(dominoSide_A.MatchId == domino.dominoSide_B.MatchId) {
        if(Math.Ceiling(Vector2.Distance(dominoSide_A.trans.position,domino.dominoSide_B.trans.position)) == distanceFromSides) {
                return true;
            }
        }
            if(dominoSide_A.MatchId == domino.dominoSide_A.MatchId) {
        if(Math.Ceiling(Vector2.Distance(dominoSide_A.trans.position,domino.dominoSide_A.trans.position)) == distanceFromSides) {
                return true;
            }
        }
            if(dominoSide_B.MatchId == domino.dominoSide_B.MatchId) {
         if(Math.Ceiling(Vector2.Distance(dominoSide_B.trans.position,domino.dominoSide_B.trans.position)) == distanceFromSides) {
                return true;
            }
        }
            if(dominoSide_B.MatchId == domino.dominoSide_A.MatchId) {
        if(Math.Ceiling(Vector2.Distance(dominoSide_B.trans.position,domino.dominoSide_A.trans.position)) == distanceFromSides) {
                return true;
            }
        }
        return false;
    }

    public void BackToTable() {
        transform.SetParent(TableParent);
        transform.localPosition = Vector3.zero;
        transform.parent.GetComponent<Slot>().Show();
        SwapOrientationToPortrait();
    }

    public void SwapOrientationToPortrait() {
        if(Orientation == EnumDominoOrientation.Landscape) {
            Orientation = EnumDominoOrientation.Portrait;
            transform.localEulerAngles = Vector2.zero;
        }
    }
}

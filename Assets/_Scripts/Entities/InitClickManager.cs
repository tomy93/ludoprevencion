﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitClickManager : MonoBehaviour {
    public static InitClickManager Instance;
    public ClickManager clickManager;

    void Awake() {
        Instance = this;
        clickManager = new ClickManager();
    }
}

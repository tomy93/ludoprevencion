using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CaseEntity : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler {
	public int id;
	public EnumCaseType caseType;
	public int typeCheckBoxIndex;
	[SerializeField]private GameObject filePrefab;
	[SerializeField]private FilePanelDisplay filePanelDisplay;
	public bool isLocked = false;

	public static CaseEntity itemBeingDragged;
	[HideInInspector]public Vector2 startPosition;
	private Transform startParent;
	private CanvasGroup cG;

	void Awake() {        
		cG = GetComponent<CanvasGroup>();

	}

	public void OnPointerClick (PointerEventData eventData)
	{
		filePanelDisplay.ListenerShowCurrentFile (filePrefab);
	}

	public void OnBeginDrag(PointerEventData eventData) {
		if(isLocked)return;
		itemBeingDragged = this;
		startPosition = GetComponent<RectTransform>().localPosition;
		startParent = transform.parent;
		cG.blocksRaycasts = false;
	}
	public void OnDrag(PointerEventData eventData) {
		if(isLocked)return;
		transform.position = Input.mousePosition;
	}  

	//later than OnDrop of IDropHandler
	public void OnEndDrag(PointerEventData eventData) {
		//if(isLocked)return;
		itemBeingDragged = null;
		cG.blocksRaycasts = true;
		if(transform.parent == startParent) {
			// transform.position = startPosition;
			GetComponent<RectTransform>().localPosition = startPosition;

		}else {
			//transform.position = transform.parent.position;            
			// GetComponent<RectTransform>().localPosition = startPosition;

		}
	}
}

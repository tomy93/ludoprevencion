using System;
using UnityEngine;
using System.Collections.Generic;
using Spine.Unity;


public class NSpineController {

	private const float DefaultFrameRate = 30.0f;

	private GameObject mGO_Spine;
	private SkeletonAnimation mSkeletonAnimation;
	private Spine.Skeleton mSkeleton;
	private Spine.TrackEntry mCurrentAnimation; 

	// main animation
	private int mCurrentFrame;
	private int mLoopCount_Main;

	private bool mIsFlippedX;
	private Vector3 mLocalScale;

	public Spine.TrackEntry CurrentAnimation 	{ get { return mCurrentAnimation; } }
	public Spine.Skeleton Skeleton 				{ get { return mSkeleton; } }
	public SkeletonAnimation SkeletonAnimation 	{ get { return mSkeletonAnimation; } }
	public GameObject GO_Spine 					{ get { return mGO_Spine; } }

	public int CurrentFrame	{ 
		get { return mCurrentFrame; } 
        // Not supported ion 3.5.x
//		set {
//			if (mCurrentAnimation != null) {
//				mCurrentFrame = value;
//				mLoopCount_Main = 0;
//
//                float totalFrames = NSpineController.DefaultFrameRate * mCurrentAnimation.AnimationEnd;
//
//                mCurrentAnimation.AnimationTime = (mCurrentAnimation.AnimationEnd * mCurrentFrame) / totalFrames;
//
//				if (mSkeletonAnimation.state.GetCurrent(0) == null && (mCurrentFrame != totalFrames)) {
//					mCurrentAnimation = mSkeletonAnimation.state.SetAnimation(0, 
//                  		mCurrentAnimation.Animation, mCurrentAnimation.Loop);
//					//mCurrentAnimation.TimeScale = 0f;
//				}
//			}
//		}
	}

	public int TotalFrames { 
		get { 
			int result = 0;

			if (mCurrentAnimation != null) {
                result = (int)(NSpineController.DefaultFrameRate * mCurrentAnimation.AnimationEnd);
			}

			return result;
		} 
	}

	public bool IsFlippedX {
		get { return mIsFlippedX; }
		set { 
			mIsFlippedX = value;
		}
	}

	public float ScaleX {
		get { return mGO_Spine.transform.localScale.x; }
		set { 
			mLocalScale.x = value;

			Vector3 localScale = mGO_Spine.transform.localScale;
			localScale.x = mLocalScale.x;
			localScale.x = localScale.x * (mIsFlippedX ? -1f : 1f);
			mGO_Spine.transform.localScale = localScale;
		}
	}

	public float ScaleY {
		get { return mGO_Spine.transform.localScale.y; }
		set { 
			mLocalScale.y = value;
			
			Vector3 localScale = mGO_Spine.transform.localScale;
			localScale.y = mLocalScale.y;
			mGO_Spine.transform.localScale = localScale;
		}
	}

	public NSpineController (string go_spine_name) {
		mGO_Spine = GameObject.Find(go_spine_name);
		mSkeletonAnimation = mGO_Spine.GetComponent<SkeletonAnimation>();
		mSkeleton = mSkeletonAnimation.skeleton;

		// main animation
		mLoopCount_Main = 1;
		mLocalScale = Vector3.one;
	}

	public NSpineController (GameObject go) {
		mGO_Spine = go;
		mSkeletonAnimation = mGO_Spine.GetComponent<SkeletonAnimation>();
		mSkeleton = mSkeletonAnimation.skeleton;

		// main animation
		mLoopCount_Main = 1;
		mLocalScale = mGO_Spine.transform.localScale;
	}

	public string[] GetAnimationNames() {
        var animations = mSkeletonAnimation.skeletonDataAsset.GetSkeletonData(true).Animations.ToArray();
        string[] result = new string[animations.Length];

        for (int i = 0; i < animations.Length; i++) {
			result[i] = animations[i].Name;
		}

		return result;
	}

	public void GotoAnimation(string animationName, bool loop) {
		mCurrentAnimation = mSkeletonAnimation.state.SetAnimation(0, animationName, loop);
		mCurrentAnimation.TimeScale = 1f;
	}

	public void GotoAnimation(string animationName, bool loop, int currentFrame) {
		mCurrentAnimation = mSkeletonAnimation.state.SetAnimation(0, animationName, loop);
		mCurrentAnimation.TimeScale = 0f;

        float totalFrames = NSpineController.DefaultFrameRate * mCurrentAnimation.AnimationEnd;

        // Not supported
        //mCurrentAnimation.AnimationTime = (mCurrentAnimation.AnimationEnd * currentFrame) / totalFrames;
	}

    // not supported
//	public void SetFrameInAnimation(int currentFrame) {
//		if (mCurrentAnimation != null) {
//            float totalFrames = NSpineController.DefaultFrameRate * mCurrentAnimation.AnimationEnd;
//
//            mCurrentAnimation.AnimationTime = (mCurrentAnimation.AnimationEnd * currentFrame) / totalFrames;
//
//			if (mSkeletonAnimation.state.GetCurrent(0) == null && 
//			    (currentFrame != totalFrames)) {
//                
//				mCurrentAnimation = mSkeletonAnimation.state.SetAnimation(0, 
//              		mCurrentAnimation.Animation, mCurrentAnimation.Loop);
//				mCurrentAnimation.TimeScale = 0f;
//			}
//		}
//	}

	private void ProcessAnimation(Spine.TrackEntry te, ref int loopCount, 
  		out int currentFrame, out bool hasEndAnimation) {

		hasEndAnimation = false;
		
        if ((te.AnimationTime / te.AnimationEnd) >= loopCount) {
			loopCount++;
			hasEndAnimation = true;
		}

		float frameRate = NSpineController.DefaultFrameRate;
        currentFrame = (int)Mathf.Ceil(((te.AnimationEnd * frameRate) - (((float)loopCount * 
            te.AnimationEnd) - te.AnimationTime) * frameRate));
	}

	public void Update(float dt) {
		if (mCurrentAnimation != null) {
			bool hasEndAnimation = false;
	
			this.ProcessAnimation(mCurrentAnimation, ref mLoopCount_Main, 
          		out mCurrentFrame, out hasEndAnimation);

			Vector3 localScale = mGO_Spine.transform.localScale;
			localScale.x = mLocalScale.x * (mIsFlippedX ? -1f : 1f);
			localScale.y = mLocalScale.y;
			mGO_Spine.transform.localScale = localScale;
		}
	}
}
using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

[ExecuteInEditMode]
public class NSpineComponent : MonoBehaviour {

	public int ForceCurrentFrame;
	public string Animation;
	public string[] AnimationNames;

	public int CurrentFrame;
	public int TotalFrames;

    private NSpineController mController;

    void Start() {
		#if UNITY_EDITOR
		mController = new NSpineController(this.gameObject);
		this.AnimationNames = mController.GetAnimationNames();
		this.Animation = this.AnimationNames[0];
		this.ForceCurrentFrame = -1;

		mController.GotoAnimation(this.Animation, true);

		UnityEditor.EditorApplication.update += this.Update;
		#endif
    }

    void Update() {
		#if UNITY_EDITOR
		if (mController != null) {
			if (this.ForceCurrentFrame != -1) {
                // Not supported
//				this.ForceCurrentFrame = this.ForceCurrentFrame < 0 ? 0 : this.ForceCurrentFrame;
//				this.ForceCurrentFrame = this.ForceCurrentFrame > this.TotalFrames ? 
//					this.TotalFrames : this.ForceCurrentFrame;
//				this.CurrentFrame = this.ForceCurrentFrame;
//
//				mController.CurrentFrame = this.CurrentFrame;
			}
			else {
				this.CurrentFrame = mController.CurrentFrame;
			}

			this.TotalFrames = mController.TotalFrames;

			mController.Update(0.01f);
			mController.SkeletonAnimation.Update(0.01f);

			//UnityEditor.SceneView.RepaintAll();
			//UnityEditor.HandleUtility.Repaint();
		}
		#endif
    }
}


skeleton.png
size: 2048,512
format: RGBA8888
filter: Linear,Linear
repeat: none
BOCA
  rotate: false
  xy: 2, 2
  size: 118, 28
  orig: 118, 28
  offset: 0, 0
  index: -1
BOCA 3
  rotate: false
  xy: 1480, 351
  size: 112, 55
  orig: 112, 55
  offset: 0, 0
  index: -1
BOCA2
  rotate: false
  xy: 348, 45
  size: 66, 36
  orig: 66, 36
  offset: 0, 0
  index: -1
BRAZO DERECHA
  rotate: true
  xy: 1025, 162
  size: 152, 154
  orig: 152, 154
  offset: 0, 0
  index: -1
BRAZO IZKIERDA
  rotate: false
  xy: 1025, 316
  size: 272, 174
  orig: 272, 174
  offset: 0, 0
  index: -1
CABEZA
  rotate: false
  xy: 348, 83
  size: 336, 407
  orig: 336, 407
  offset: 0, 0
  index: -1
CEJAS
  rotate: false
  xy: 686, 115
  size: 162, 52
  orig: 162, 52
  offset: 0, 0
  index: -1
LUPA
  rotate: true
  xy: 1299, 321
  size: 85, 179
  orig: 85, 179
  offset: 0, 0
  index: -1
NARIZ
  rotate: false
  xy: 1181, 188
  size: 77, 126
  orig: 77, 126
  offset: 0, 0
  index: -1
OJO DERECHA
  rotate: false
  xy: 1798, 461
  size: 30, 29
  orig: 30, 29
  offset: 0, 0
  index: -1
OJO IZKIERDA
  rotate: false
  xy: 686, 83
  size: 27, 30
  orig: 27, 30
  offset: 0, 0
  index: -1
PARPADOS
  rotate: false
  xy: 1650, 417
  size: 146, 73
  orig: 146, 73
  offset: 0, 0
  index: -1
PIERNAS
  rotate: false
  xy: 2, 32
  size: 344, 458
  orig: 344, 458
  offset: 0, 0
  index: -1
SACO INFERIOR
  rotate: false
  xy: 686, 169
  size: 189, 321
  orig: 189, 321
  offset: 0, 0
  index: -1
SACO SUPERIOR
  rotate: false
  xy: 877, 173
  size: 146, 317
  orig: 146, 317
  offset: 0, 0
  index: -1
SOMBRA
  rotate: false
  xy: 1299, 408
  size: 349, 82
  orig: 349, 82
  offset: 0, 0
  index: -1

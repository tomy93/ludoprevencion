using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShuffleRectTPositions : MonoBehaviour {
    [SerializeField]private bool shuffleOnAwake = true;
    [SerializeField]private RectTransform[] aRTs;
   
    private Transform[] aParents;

    void Start() {
        if(DominosValues.FirstTimePlaying == false) {
            return;
        }
		if(shuffleOnAwake) {
            aParents = new Transform[aRTs.Length];
            for(int i = 0; i < aParents.Length; i++) {
                aParents[i] = aRTs[i].parent;
            }
            shuffleSlots();
            setNewPositions();
        }
	}

    private void setNewPositions() {
        DominosValues.aDomPosVec = new DominoPositionVector[aRTs.Length];

        for(int i = 0; i < aRTs.Length; i++) {
            aRTs[i].SetParent(aParents[i]);
            aRTs[i].localPosition = Vector3.zero;

            DominosValues.aDomPosVec[i] = new DominoPositionVector();
            DominosValues.aDomPosVec[i].DominoID = aRTs[i].GetComponent<DominoEntity>().dominoID;          
            DominosValues.aDomPosVec[i].SlotID = aRTs[i].parent.GetComponent<Slot>().SlotID;   
           // Debug.Log(DominosValues.aDomPosVec[i].SlotID+ "____" + DominosValues.aDomPosVec[i].DominoID );      
        }     
        
    }

    private void shuffleSlots() {
        for(int i = 0; i < aRTs.Length; i++) {
            RectTransform rt = aRTs[i];
            int r = Random.Range(i, aRTs.Length);
            aRTs[i] = aRTs[r];
            aRTs[r] = rt;
        }
    }

    void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinchZoom : MonoBehaviour
	{
	private bool canZoom;

	void OnEnable(){
		Input.multiTouchEnabled = true;
	}
	void OnDisable(){
		Input.multiTouchEnabled = false;
	}

	public void ListenerEnableZoom(){
		canZoom = true;
	}

	public void ListenerDisableZoom(){
		canZoom = false;
	}

	public void ListenerReset(){
		transform.localScale = Vector3.one;
	}

		void Update()
		{

		if (canZoom == false)
			return;
			// If there are two touches on the device...
			if (Input.touchCount == 2)
			{
				// Store both touches.
				Touch touchZero = Input.GetTouch(0);
				Touch touchOne = Input.GetTouch(1);

				// Find the position in the previous frame of each touch.
				Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
				Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

				// Find the magnitude of the vector (the distance) between the touches in each frame.
				float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
				float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

				// Find the difference in the distances between each frame.
				float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

				transform.localScale = transform.localScale - Vector3.one * deltaMagnitudeDiff / 1000;

			}
		}
	}


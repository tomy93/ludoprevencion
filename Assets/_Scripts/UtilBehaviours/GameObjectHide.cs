using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectHide : MonoBehaviour {

	public void ListenerHide() {
        gameObject.SetActive(false);
    }
}

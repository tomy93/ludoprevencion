using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShuffleSolitaryCards : MonoBehaviour {
	[Header("Table has to have Layout Group disabled to work")]
    [SerializeField]private bool shuffleOnAwake = true;
    [SerializeField]private RectTransform[] aRTs;
    private Vector2[] aNewPositions;

	void Awake () {
		if(!shuffleOnAwake) return;
        shuffleCards();
		
	}

    private void shuffleCards() {
        //save positions in another array
        aNewPositions = new Vector2[aRTs.Length];
        for(int i = 0; i < aNewPositions.Length; i++) {
            aNewPositions[i] = aRTs[i].localPosition;
        }
        //shuffle array with positions
        for(int i = 0; i < aNewPositions.Length; i++) {
            Vector2 pos = aNewPositions[i];
            int r = Random.Range(i, aNewPositions.Length);
            aNewPositions[i] = aNewPositions[r];
            aNewPositions[r] = pos;
        }
        //replace positions
        for(int i = 0; i < aRTs.Length; i++) {
            aRTs[i].localPosition = aNewPositions[i];
        }
        Debug.Log("shuffleados");
    }

    void Update () {
		
	}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlwaysAlive : MonoBehaviour {

	void Awake () {
		GameObject.DontDestroyOnLoad(gameObject);
	}
	
	
}

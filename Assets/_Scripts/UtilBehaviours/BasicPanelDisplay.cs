using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicPanelDisplay : MonoBehaviour {
    private bool firstTime;

	void Awake() {
        if(firstTime == true)return;
        gameObject.SetActive(false);
    }
	public void ListenerSwapActive() {
        firstTime = true;
        gameObject.SetActive(!isActiveAndEnabled);
    }
}

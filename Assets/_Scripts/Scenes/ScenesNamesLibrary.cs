public class ScenesNamesLibrary
{
    
    public const string LOADING = "sc_LoadingScene";
    public const string DOMINO = "sc_g_Dominos";
    public const string SOLITARY = "sc_g_Solitary";
    public const string ACCIDENT_CASE = "sc_g_AccidentCase";
    public const string HOME_MENU = "sc_HomeMenu";
    public const string HIGHSCORES = "sc_Highscores";
}

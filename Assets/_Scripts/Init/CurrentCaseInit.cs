using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CaseAnswer {
    public EnumCaseType caseType;
    public int index;
}

public class CurrentCaseInit : MonoBehaviour {
    [SerializeField] private GameObject[] aCasePrefabs;
    [SerializeField] private FilePanelDisplay filePanel;
	public CaseAnswer[] aCase1CorrectAnswers;
	public CaseAnswer[] aCase2CorrectAnswers;
    public CaseAnswer[] aCase3CorrectAnswers;
	[HideInInspector] public int CaseUsed;

    void Awake() {
        //random case
		//CaseUsed = Random.Range(0, aCasePrefabs.Length);
		CaseUsed = 2;
		Debug.Log ("CaseUsed " + CaseUsed);
		filePanel.ListenerShowCurrentFile(aCasePrefabs[CaseUsed]);
    }
	void Start () {
		
	}
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//In games 1 & 2 I need cameras, so, the first main camera (at LoadingScene) is making problems
public class MainCameraEnable : MonoBehaviour {
	[SerializeField] private bool enable;

	void OnEnable(){
		AudioPlayer.Instance.EnableMainCamera (enable);
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

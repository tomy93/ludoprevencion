using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AllScenesLoad : MonoBehaviour {
    [SerializeField] private bool multitouchEnabled = false;

    private Scene sc0;
    private Scene sc1;
    private Scene sc2;
    private Scene sc3;
    private Scene sc4;

	void Awake() {
        if(!multitouchEnabled) {
            Input.multiTouchEnabled = false;
        }
        loadAllScenes();
    }

    private void loadAllScenes() {
        SceneManager.LoadScene(ScenesNamesLibrary.HIGHSCORES,LoadSceneMode.Additive);
        SceneManager.LoadScene(ScenesNamesLibrary.DOMINO,LoadSceneMode.Additive);
        SceneManager.LoadScene(ScenesNamesLibrary.SOLITARY,LoadSceneMode.Additive);
        SceneManager.LoadScene(ScenesNamesLibrary.ACCIDENT_CASE,LoadSceneMode.Additive);
        SceneManager.LoadScene(ScenesNamesLibrary.HOME_MENU,LoadSceneMode.Additive);    

        sc0 = SceneManager.GetSceneByName(ScenesNamesLibrary.HOME_MENU);
        sc1 = SceneManager.GetSceneByName(ScenesNamesLibrary.HIGHSCORES);
        sc2 = SceneManager.GetSceneByName(ScenesNamesLibrary.DOMINO);
        sc3 = SceneManager.GetSceneByName(ScenesNamesLibrary.SOLITARY);
        sc4 = SceneManager.GetSceneByName(ScenesNamesLibrary.ACCIDENT_CASE);

        if(FadeDisplay.Instance)FadeDisplay.Instance.Show();
     //   SceneManager.sceneLoaded+= sceneLoaded;

    }

    void Update() {
         if(sc0.isLoaded) {
            for(int i = 0; i < sc0.GetRootGameObjects().Length; i++) {
                sc0.GetRootGameObjects()[i].gameObject.SetActive(false);
            }
            SceneSwitch.GotoMainMenu();
        }        
        if(sc1.isLoaded) {
            for(int i = 0; i < sc1.GetRootGameObjects().Length; i++) {
                sc1.GetRootGameObjects()[i].gameObject.SetActive(false);
            }
        }
        if(sc2.isLoaded) {
            for(int i = 0; i < sc2.GetRootGameObjects().Length; i++) {
                sc2.GetRootGameObjects()[i].gameObject.SetActive(false);
            }
        }
        if(sc3.isLoaded) {
            for(int i = 0; i < sc3.GetRootGameObjects().Length; i++) {
                sc3.GetRootGameObjects()[i].gameObject.SetActive(false);
            }
        }
        if(sc4.isLoaded) {
            for(int i = 0; i < sc4.GetRootGameObjects().Length; i++) {
                sc4.GetRootGameObjects()[i].gameObject.SetActive(false);
            }
        }
    }

    private void sceneLoaded(Scene arg0, LoadSceneMode arg1) {

        Debug.Log("scene loaded " + arg0.name + arg0.isLoaded);
        if(arg0.name != ScenesNamesLibrary.HOME_MENU && arg0.name != ScenesNamesLibrary.LOADING) {
            Debug.Log("entr�");
            for(int i = 0; i < arg0.GetRootGameObjects().Length; i++) {
                arg0.GetRootGameObjects()[i].gameObject.SetActive(false);
            }
        }
    }
}

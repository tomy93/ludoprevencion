using UnityEngine;

public class CaseGameValuesConfig:CaseGameValues{
    
    public static void SetCurrentTimeLeft(int time) {
        CurrentTimeLeft = time;
    }

    public static void CountValidation() {
        Debug.Log("***endValidation");
        CurrentValidations++;
    }

	public static void DoneIncorrectMove(){
		IncorrectMoves++;
		Debug.Log("***DoneIncorrectMove!!");
	}

	public static void DoneCorrectMove() {
		CorrectMoves++;
		Debug.Log("***DoneCorrectMove!!");
	}

    public static void Lose() {
		Debug.Log("***Perdiste!!");        
    }

    public static void EndGame() {
        FinalScore = CurrentTimeLeft + CorrectMoves - IncorrectMoves;
        Debug.Log("***GG FinalScore " + FinalScore);
        GameValues.FinalScore = FinalScore + DominosValues.FinalScore + SolitaryValues.FinalScore;
        CaseActions.OnGameEnd.Invoke();
    }

    public static void Reset() {
		GameEnded = false;
        CurrentTimeLeft = 0;
        CurrentValidations = 0;
        CorrectMoves = 0;
        IncorrectMoves = 0;
        FinalScore = 0;
    }

}

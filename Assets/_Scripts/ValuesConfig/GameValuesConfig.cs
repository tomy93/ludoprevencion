public class GameValuesConfig : GameValues {

    public static void ResetGame() {
        FinalScore = 0;
        CurrentUser = null;
        CaseGameValuesConfig.Reset();
        DominosValuesConfig.Reset();
        SolitaryValuesConfig.Reset();
    }

}

using System;
using UnityEngine;

class SolitaryValuesConfig:SolitaryValues {

    //match
    public static void DoneCorrectMove() {
        Debug.Log("***DoneCorrectMove!!");
        CurrentIncorrectMoves = 0;
        CorrectMoves++;
        if(AudioPlayer.Instance)AudioPlayer.Instance.PlaySetCard();

        if(CorrectMoves == TOTAL_CARDS_AMOUNT) {
            Win();
        }
    }

    //notMatch
	public static void DoneIncorrectMove(out bool x) {
		x = false;
        CurrentIncorrectMoves++;
        GlobalIncorrectMoves++;
        if(AudioPlayer.Instance)AudioPlayer.Instance.PlayBadFeedback();
        Debug.Log("***DoneIncorrectMove!!");
        if(CurrentIncorrectMoves == TOTAL_CURRENT_INCORRECT_MOVES) {
			Debug.Log("***ShowMessage!!");
			x = true;
            CurrentIncorrectMoves = 0;
        }
    }

    internal static void Win() {
        Debug.Log("***YOU WIN!!!"); 
		GameEnded = true;
        FinalScore = CorrectMoves - GlobalIncorrectMoves;
        SolitaryActions.OnGameEnd.Invoke();
    }

	public static void Reset() {
		GameEnded = false;
        CurrentIncorrectMoves = 0;
        GlobalIncorrectMoves = 0;
        FinalScore = 0;
        CorrectMoves = 0;
    }
}

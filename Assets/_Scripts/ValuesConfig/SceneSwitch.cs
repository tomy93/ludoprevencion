using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class SceneSwitch {
    public static string currentScene;
    public static string lastScene;

    public static void GotoNextScene() {
        Scene sc = SceneManager.GetSceneByName(currentScene); 
        if(sc.isLoaded) {
            deactiveGameObjects();
            activeGameObjects();
        }else {
            deactiveGameObjects();
            SceneManager.LoadScene(currentScene, LoadSceneMode.Additive);
        }

        lastScene = currentScene;
    }

	public static void GotoMainMenu(string message = ""){
        debug(message);
        if(FadeDisplay.Instance)FadeDisplay.Instance.Show();
        currentScene = ScenesNamesLibrary.HOME_MENU;       
    }
    public static void GotoHighscores(string message = "")
    {
        debug(message);
        if(FadeDisplay.Instance)FadeDisplay.Instance.Show();
        currentScene = ScenesNamesLibrary.HIGHSCORES;       
    }

    public static void GotoDomino(string message = "")
    {
        debug(message);       
        if(FadeDisplay.Instance)FadeDisplay.Instance.Show();
        currentScene = ScenesNamesLibrary.DOMINO;          
    }
    public static void GotoSolitary(string message = "")
    {
        DominosValues.FirstTimePlaying = false;
        debug(message);
        if(FadeDisplay.Instance)FadeDisplay.Instance.Show();
        currentScene = ScenesNamesLibrary.SOLITARY;       
    }
    public static void GotoAccidentCase(string message = "")
    {
        debug(message);
        if(FadeDisplay.Instance)FadeDisplay.Instance.Show();
        currentScene = ScenesNamesLibrary.ACCIDENT_CASE;        
    }   

    private static void deactiveGameObjects() {
        GameObject[] aGo = null;
        //lastScene is null the firstime
        if(lastScene == null) {
            aGo = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
        }else {
            aGo = UnityEngine.SceneManagement.SceneManager.GetSceneByName(lastScene).GetRootGameObjects();
        }
        for(int i = 0; i < aGo.Length; i++) {
            aGo[i].SetActive(false);
        }        
    }   

    private static void activeGameObjects() {
        GameObject[] aGo = UnityEngine.SceneManagement.SceneManager.GetSceneByName(currentScene).GetRootGameObjects();
        for(int i = 0; i < aGo.Length; i++) {
            aGo[i].SetActive(true);
        }  
    }

    private static void debug(string message)
    {
        if(FadeDisplay.Instance) {
            Debug.Log("***SceneSwitch " + message);
        }else {
            Debug.Log("***SceneSwitch attemp!!!");
        }

    }
}

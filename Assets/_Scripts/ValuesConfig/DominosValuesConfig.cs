using System;
using UnityEngine;

class DominosValuesConfig:DominosValues {

    //match
    public static void DoneCorrectMove() {
        Debug.Log("***Done Correct Move!!");
        CurrentIncorrectMoves = 0;
        CorrectMoves++;
        if(AudioPlayer.Instance)AudioPlayer.Instance.PlaySetDomino();

        //menos 1 porque la primera ficha que pones no se cuenta como correct move
        if(CorrectMoves == TOTAL_DOMINOS_AMOUNT-1) {
            Win();
        }
    }

    //notMatch
	public static void DoneIncorrectMove(out bool x) {
        Debug.Log("***Done Incorrect Move!!");
		x = false;
        CurrentIncorrectMoves++;
        GlobalIncorrectMoves++;
        if(AudioPlayer.Instance)AudioPlayer.Instance.PlayBadFeedback();
        if(CurrentIncorrectMoves == TOTAL_CURRENT_INCORRECT_MOVES) {
            Debug.Log("***show panel message of 'you are doing wrong!!'");
			//HelpMessageDisplay.Instance.DisplayMessage ();
			x = true;
            CurrentIncorrectMoves = 0;
        }
    }

    internal static void Win() {
		Debug.Log("***YOU WIN!!!"); 
		GameEnded = true;
        FinalScore = CorrectMoves - GlobalIncorrectMoves;
        DominosActions.OnGameEnd.Invoke();
    }

	public static void Reset() {
		GameEnded = false;
        FirstTimePlaying = true;
        aDomPosVec = null;
        CurrentDominoIdDragged = -1;
        LastDominoDragged = null;
        lDomTable = null;
        lDomBoard = null;
        CurrentIncorrectMoves=0;
        GlobalIncorrectMoves=0;
        FinalScore=0;
        CorrectMoves=0;
    }
}

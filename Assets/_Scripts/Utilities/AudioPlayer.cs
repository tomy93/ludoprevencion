using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour {
    private static AudioPlayer instance;
	[SerializeField] private GameObject MainCamera;
    [SerializeField] private AudioSource auButton;
    [SerializeField] private AudioSource auSetCard;
    [SerializeField] private AudioSource auSetDomino;
    [SerializeField] private AudioSource auBadFeedback;

    public static AudioPlayer Instance {
        get {
            if(instance) {
                return instance;
            }else {
                Debug.LogWarning("***No hay instancia de AudioPlayer");
                return null;
            }
        }

        set {
            instance = value;
        }
    }

    void Awake() {
        Instance = this;
    }

	public void EnableMainCamera(bool active){
		MainCamera.SetActive (active);
	}

    public void PlayButton() {
        auButton.Play();
    }
    public void PlaySetCard() {
        auSetCard.Play();
    }
    public void PlaySetDomino() {
        auSetDomino.Play();
    }
    public void PlayBadFeedback() {
        auBadFeedback.Play();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneSwitchListener : MonoBehaviour{
    [SerializeField] private bool debugMode = true;

	public void ListenerGotoMainMenu(string message)
    {
        if(AudioPlayer.Instance)AudioPlayer.Instance.PlayButton();
        if (!debugMode) message = "";
        SceneSwitch.GotoMainMenu(message);
    }
    public void ListenerGotoHighscores(string message)
    {
        if(AudioPlayer.Instance)AudioPlayer.Instance.PlayButton();
        if (!debugMode) message = "";
        SceneSwitch.GotoHighscores(message);
    }

    public void ListenerGotoDomino(string message)
    {
        if(AudioPlayer.Instance)AudioPlayer.Instance.PlayButton();
        if (!debugMode) message = "";
        SceneSwitch.GotoDomino(message);
    }

    public void ListenerGotoSolitary(string message)
    {
        if(AudioPlayer.Instance)AudioPlayer.Instance.PlayButton();
        if (!debugMode) message = "";
        SceneSwitch.GotoSolitary(message);
    }

    public void ListenerGotoAccidentCase(string message)
    {
        if(AudioPlayer.Instance)AudioPlayer.Instance.PlayButton();
        if (!debugMode) message = "";
        SceneSwitch.GotoAccidentCase(message);
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DominosActions : MonoBehaviour {

    public static Action OnTableOrderChanged;
    public static Action OnBeginDragFromTable;
    public static Action OnEndDragFromTable;
    public static Action OnDropInTable;

    public static Action OnGameEnd;
    
    
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CaseActions : MonoBehaviour {

    public static Action OnGameEnd;
    [SerializeField]private UnityEvent onGameEnd;

    void OnEnable() {
        OnGameEnd+= onGameEnded;
    }
    void OnDisable() {
        OnGameEnd-= onGameEnded;
    }
    private void onGameEnded() {
        onGameEnd.Invoke();
    }
}

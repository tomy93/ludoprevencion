﻿using UnityEngine;

[System.Serializable]
public struct DominoSide {
        public int MatchId;
        public string Text;
        public int PointsValue;
        public Transform trans;
}
	

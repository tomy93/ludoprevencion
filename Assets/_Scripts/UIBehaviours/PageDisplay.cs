﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PageDisplay : MonoBehaviour {
	[SerializeField] private GameObject[] aPages;
	[SerializeField] private Text txtPageNumber;
	private int currentPage=0;

	void Awake(){
		show ();
		showText ();
	}
	private void show(){
		for (int i = 0; i < aPages.Length; i++) {
			if (i == currentPage) {
				aPages [i].SetActive (true);
			} else {
				aPages [i].SetActive (false);
			}
		}
	}

	public void ListenerNextPage(){
		currentPage++;
		if (currentPage > aPages.Length-1) {
			currentPage = 0;
		}
		show ();
		showText ();
	}

	public void ListenerPrevPage(){
		currentPage--;
		if (currentPage < 0) {
			currentPage = aPages.Length-1;
		}
		show ();
		showText ();
	}

	public void showText(){
		txtPageNumber.text = currentPage + 1 + "/" + aPages.Length;
	}
}

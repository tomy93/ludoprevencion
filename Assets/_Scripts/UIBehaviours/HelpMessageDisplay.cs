﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class HelpMessageDisplay : MonoBehaviour {
	[SerializeField] private float maxTimeShowing = 4.0f;
	[SerializeField] private GameObject goMessage;
	[SerializeField] private SkeletonAnimation skAnimation;

	private float timeCounter;
	private bool isShowing;

	void Awake(){
		skAnimation.gameObject.SetActive (false);
		goMessage.gameObject.SetActive (false);
		timeCounter = 0;
		isShowing = false;
	}

	public void DisplayMessage(){
		isShowing = true;
		skAnimation.Initialize (true);
		skAnimation.gameObject.SetActive (true);
		goMessage.gameObject.SetActive (true);
		timeCounter = 0;
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!isShowing) {return;
		}
		timeCounter += Time.deltaTime;
		if (timeCounter >= maxTimeShowing) {
			Awake ();
		}

	}
}

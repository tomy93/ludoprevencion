using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserInfoPanelFill : MonoBehaviour {
    [SerializeField] private Text txtName;
    [SerializeField] private Text txtCountry;
    [SerializeField] private GameObject invalidMessage;

    private void OnEnable() {
        invalidMessage.gameObject.SetActive(false);
    }

    public void ListenerValidateData() {
        if(txtName.text != "") {

        }else {
            showInvalidMessage();
            return;
        }

        if(txtCountry.text != "") {

        }else {
            showInvalidMessage();
            return;
        }

        passInfo();
    }

    private void passInfo() {
        invalidMessage.gameObject.SetActive(false);
        GameValues.CurrentUser = new User(0, txtName.text, txtCountry.text, GameValues.FinalScore);
        gameObject.SetActive(false);
        HighScoresActions.OnInfoFill.Invoke();
    }

    private void showInvalidMessage() {
        invalidMessage.gameObject.SetActive(true);
    }
}

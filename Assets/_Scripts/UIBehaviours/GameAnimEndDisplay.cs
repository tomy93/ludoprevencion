using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]

public class GameAnimEndDisplay : MonoBehaviour {
    [SerializeField] private float timeToChangeScene=3;
    private Transform child;
    private Animator animator;
    [SerializeField]private Text txtPointsDominos;
    [SerializeField]private Text txtPointsSolitary;
    [SerializeField]private Text txtPointsCaseGame;
    [SerializeField]private Text txtPointsTotal;
    
    void Awake() {
        child = transform.GetChild(0);
        child.gameObject.SetActive(false);
        animator = GetComponent<Animator>();        
    }

	void OnEnable() {
        child.gameObject.SetActive(false);
        CaseActions.OnGameEnd+=showAnimation;
       
    }
    void OnDisable() {
        CaseActions.OnGameEnd-=showAnimation;       
    }  

    private void showAnimation() {
        txtPointsDominos.text = DominosValues.FinalScore + " puntos.";
        txtPointsSolitary.text = SolitaryValues.FinalScore + " puntos.";
        txtPointsCaseGame.text = CaseGameValues.FinalScore + " puntos.";
        txtPointsTotal.text = GameValues.FinalScore + " puntos.";

        child.gameObject.SetActive(true);
        Invoke("endGame",timeToChangeScene);
        animator.Play("gameEnd_intro");
    }

    private void endGame() {
        //child.gameObject.SetActive(false);
        SceneSwitch.GotoHighscores();
    }
}

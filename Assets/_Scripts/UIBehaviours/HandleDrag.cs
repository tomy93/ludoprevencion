using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HandleDrag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    public static GameObject itemBeingDragged;
    private Vector3 startPosition;
    private Transform startParent;
    private CanvasGroup cG;

    void Awake() { 
        cG = GetComponent<CanvasGroup>();
    }

    public void OnBeginDrag(PointerEventData eventData) {
        //Debug.Log("locked::: " + GetComponent<DominoEntity>().Locked);
        if(GetComponent<DominoEntity>().Locked)return;

        DominosValues.CurrentDominoIdDragged = GetComponent<DominoEntity>().dominoID;
        DominosValues.LastDominoDragged = gameObject.GetComponent<DominoEntity>();
        if(transform.parent.GetComponent<Slot>().Location == EnumDominoLocations.Table) {
            GetComponent<DominoEntity>().TableParent = transform.parent;
            DominosActions.OnBeginDragFromTable.Invoke();
            transform.parent.GetComponent<Slot>().Hide();
        }       

        itemBeingDragged = gameObject;
        startPosition = transform.position;
        startParent = transform.parent;
        cG.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData) {
        if(GetComponent<DominoEntity>().Locked)return;

		//when canvas is screen space overlay:
        //transform.position = Input.mousePosition;

		//when canvas is screen space camera:
		Vector3 screenPoint = Input.mousePosition;
		screenPoint.z = 100f; //distance of the plane from the camera
		transform.position = Camera.main.ScreenToWorldPoint(screenPoint);

        transform.localScale = Vector3.one;//this is because sometimes when changing parents it sets to 1.0000001f; :c
    }

    public void OnEndDrag(PointerEventData eventData) { 
        if(GetComponent<DominoEntity>().Locked)return; 
        //if backs to table and is in landscape, so set it to portrait;  
        if(transform.parent.GetComponent<Slot>().Orientation != GetComponent<DominoEntity>().Orientation) {
            transform.localEulerAngles = Vector2.zero;
            transform.GetComponent<DominoEntity>().Orientation = EnumDominoOrientation.Portrait;
        }


        itemBeingDragged = null;
        cG.blocksRaycasts = true;
        if(transform.parent == startParent) {
            GetComponent<DominoEntity>().SwapOrientationToPortrait();
            transform.position = startPosition;
            transform.parent.GetComponent<Slot>().Show();
        }else {
            transform.position = transform.parent.position;            
        }
        GetComponent<DominoEntity>().TableParent.GetComponent<Slot>().SwapOrientationToPortrait();

        DominosActions.OnEndDragFromTable.Invoke();

    }
}

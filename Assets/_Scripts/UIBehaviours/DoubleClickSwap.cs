using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleClickSwap : MonoBehaviour {
    private Slot slot;

    public void Awake() {
        slot = GetComponent<Slot>();
    }

    public void ListenerDoubleClick() {
        if(InitClickManager.Instance.clickManager.DoubleClick()) {
            Rotate();            
        }
    }

    public void Rotate() {
        if(slot.Location == EnumDominoLocations.Table) {
            slot.SwapOrientation();
            transform.Rotate(new Vector3(0,0,-90));
        }
    }

    internal void RotateFromTable() {
        slot.SwapOrientation();
            transform.Rotate(new Vector3(0,0,-90));
    }
}

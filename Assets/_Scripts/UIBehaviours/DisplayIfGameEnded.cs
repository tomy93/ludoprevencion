﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayIfGameEnded : MonoBehaviour {
	[SerializeField] private EnumGames currentGame;

	void Awake(){
		switch (currentGame) {
		case EnumGames.Domino:
			if (!DominosValues.GameEnded) {
				gameObject.SetActive (false);
			}
			break;
		case EnumGames.Solitary:
			if (!SolitaryValues.GameEnded) {
				gameObject.SetActive (false);
			}
			break;
		default:
			break;
		}
	}
}

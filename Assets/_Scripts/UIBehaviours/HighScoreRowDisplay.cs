using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreRowDisplay : MonoBehaviour {
    public Text txtName;
    public Text txtScore;
    public Text txtCountry;
    [HideInInspector]public int Score = 0;


    public void SetInfo(string _name,string _country, int _score) {
        txtName.text = _name;
        txtScore.text = _score.ToString();
        Score = _score;
        txtCountry.text = _country;
    }
}

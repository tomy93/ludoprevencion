using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameScenesReload : MonoBehaviour {
    
    private Scene sc0;
    private Scene sc1;
    private Scene sc2;
    private Scene sc3;
    private Scene sc4;
    private Scene sc5;

    private bool canVerifyUnloaded = false;
    private bool canVerifyLoaded = false;

    private void OnEnable() {
		GameValuesConfig.ResetGame ();

        canVerifyUnloaded = false;
        canVerifyLoaded = false;

        if(SceneSwitch.lastScene == ScenesNamesLibrary.DOMINO||
            SceneSwitch.lastScene == ScenesNamesLibrary.SOLITARY||
            SceneSwitch.lastScene == ScenesNamesLibrary.ACCIDENT_CASE) {
            Invoke("UnloadGameScenes",0.1f);
            //UnloadGameScenes();
        }
    }

    public void UnloadGameScenes() {    
        canVerifyUnloaded = true;

        sc0 = SceneManager.GetSceneByName(ScenesNamesLibrary.DOMINO);
        sc1 = SceneManager.GetSceneByName(ScenesNamesLibrary.SOLITARY);
        sc2 = SceneManager.GetSceneByName(ScenesNamesLibrary.ACCIDENT_CASE);
        
        //se rompe si se usa UnloadSceneAsync, ya que si vuelvo a cargar la misma escena, la carga pero nunca cambia su isLoaded a true.
        SceneManager.UnloadScene(ScenesNamesLibrary.DOMINO);
        SceneManager.UnloadScene(ScenesNamesLibrary.SOLITARY);
        SceneManager.UnloadScene(ScenesNamesLibrary.ACCIDENT_CASE);
      

    }

    private void Update() {
        if(canVerifyUnloaded) {
            if(sc0.isLoaded == false && sc1.isLoaded == false && sc2.isLoaded == false) {
                canVerifyUnloaded = false;
                canVerifyLoaded = true;
                reloadGameScenes();
            }
        }

        if(canVerifyLoaded) {
            if(sc3.isLoaded) {
                for(int i = 0; i < sc3.GetRootGameObjects().Length; i++) {
                    sc3.GetRootGameObjects()[i].gameObject.SetActive(false);
                }
            }        
            if(sc4.isLoaded) {
                for(int i = 0; i < sc4.GetRootGameObjects().Length; i++) {
                    sc4.GetRootGameObjects()[i].gameObject.SetActive(false);
                }
            }
            if(sc5.isLoaded) {
                for(int i = 0; i < sc5.GetRootGameObjects().Length; i++) {
                    sc5.GetRootGameObjects()[i].gameObject.SetActive(false);
                }
            }
            if(sc3.isLoaded == true && sc4.isLoaded == true && sc5.isLoaded == true) { 
                canVerifyLoaded = false;

            }
        }
        
    }

    private void reloadGameScenes() {
         
        SceneManager.LoadScene(ScenesNamesLibrary.DOMINO,LoadSceneMode.Additive);
        SceneManager.LoadScene(ScenesNamesLibrary.SOLITARY,LoadSceneMode.Additive);
        SceneManager.LoadScene(ScenesNamesLibrary.ACCIDENT_CASE,LoadSceneMode.Additive);

        
        sc3 = SceneManager.GetSceneByName(ScenesNamesLibrary.DOMINO);
        sc4 = SceneManager.GetSceneByName(ScenesNamesLibrary.SOLITARY);
        sc5 = SceneManager.GetSceneByName(ScenesNamesLibrary.ACCIDENT_CASE);
        
        canVerifyLoaded = true;
    }
}

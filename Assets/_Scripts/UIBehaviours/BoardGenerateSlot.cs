using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BoardGenerateSlot : MonoBehaviour {
    [SerializeField]
    private GameObject slotPrefab;
    private List<GameObject> lTempSlots;
    private List<GameObject> lSlots;   

	[SerializeField]private bool hideSlots=true;
	[SerializeField]private HelpMessageDisplay helpMessage;
    private float factor;

    void Start() {
        //DominosActions.OnBeginDragFromTable += showPosibleLocations;
        //DominosActions.OnEndDragFromTable += checkIfNewSlotIsFilled;
        //DominosActions.OnDropInTable += checkIfSlotsAreEmpty;
        lTempSlots = new List<GameObject>();
        lSlots = new List<GameObject>();
        factor = DominosValues.DOMINO_SIZE_FACTOR;
    }
    void OnEnable() {
        DominosActions.OnBeginDragFromTable += showPosibleLocations;
        DominosActions.OnEndDragFromTable += checkIfNewSlotIsFilled;
        DominosActions.OnDropInTable += checkIfSlotsAreEmpty;
    }
    void OnDisable() {
        DominosActions.OnBeginDragFromTable -= showPosibleLocations;
        DominosActions.OnEndDragFromTable -= checkIfNewSlotIsFilled;
        DominosActions.OnDropInTable -= checkIfSlotsAreEmpty;
    }

    private bool isFirstTurn() {
        if(lSlots.Count == 0) {
            return true;
        } else
            return false;
    }

    private void showPosibleLocations() {
        if(isFirstTurn()) {
            if(DominosValues.LastDominoDragged.Orientation == EnumDominoOrientation.Portrait) {
                lTempSlots.Add((GameObject)Instantiate(slotPrefab, transform, false));
                lTempSlots[0].transform.localPosition = new Vector3(-100,0,0);
                lTempSlots[0].name = "new Slot";
                lTempSlots[0].GetComponent<Slot>().Orientation = EnumDominoOrientation.Portrait;
                lTempSlots[0].GetComponent<Slot>().Location = EnumDominoLocations.Board;
            }else if(DominosValues.LastDominoDragged.Orientation == EnumDominoOrientation.Landscape) {    
                lTempSlots.Add((GameObject)Instantiate(slotPrefab, transform, false));
                lTempSlots[0].transform.localPosition = new Vector3(-100,0,0);
                lTempSlots[0].name = "new Slot";
                lTempSlots[0].GetComponent<DoubleClickSwap>().Rotate();
                lTempSlots[0].GetComponent<Slot>().Location = EnumDominoLocations.Board;
            }
        } else {
            if(DominosValues.LastDominoDragged.Orientation == EnumDominoOrientation.Portrait) {
                showPosibleLocationsPortrait();
            }else if(DominosValues.LastDominoDragged.Orientation == EnumDominoOrientation.Landscape) {                
                showPosibleLocationsLandscape();
            }                   
        }
        counter ++;
       // if(counter == 3)Debug.Break();
    }

    private void showPosibleLocationsLandscape() {
        for(int i = 0; i < lSlots.Count; i++) {
            if(lSlots[i].GetComponent<Slot>().Orientation == EnumDominoOrientation.Landscape) {
                for(int a = 0; a < 6; a++) {
                    GameObject go = (GameObject)Instantiate(slotPrefab, transform, false);
                    go.name = "new Slot";
                    go.GetComponent<DoubleClickSwap>().RotateFromTable();
                    go.GetComponent<Slot>().Location = EnumDominoLocations.Board;
                    go.GetComponent<RectTransform>().localPosition = nextPositionFromSlotInBoardLandscapeWithDominoLanscape(lSlots[i].transform, a);                    
                    if(hideSlots)go.GetComponent<Image>().color = new Color(0,0,0,0);
                    lTempSlots.Add(go);  
                }
            }else if(lSlots[i].GetComponent<Slot>().Orientation == EnumDominoOrientation.Portrait) {
                for(int a = 0; a < 8; a++) {
                    GameObject go = (GameObject)Instantiate(slotPrefab, transform, false);
                    go.name = "new Slot";
                    go.GetComponent<Slot>().Location = EnumDominoLocations.Board;
                    go.GetComponent<DoubleClickSwap>().RotateFromTable();
                    go.GetComponent<RectTransform>().localPosition = nextPositionFromSlotInBoardPortraitWithDominoLanscape(lSlots[i].transform, a);
                   if(hideSlots) go.GetComponent<Image>().color = new Color(0,0,0,0);
                    lTempSlots.Add(go);  
                }
            }
           
        }
        //hide slots over other things(slots or dominoes)
        //hide slots with dominos
        for(int b = 0; b < lSlots.Count; b++) {
            for(int c = 0; c < lTempSlots.Count; c++) {
                if(lSlots[b].transform.position == lTempSlots[c].transform.position) {
                    lTempSlots[c].GetComponent<Slot>().enabled = false; lTempSlots[c].gameObject.SetActive(false);
                }
            }
        }
        //hide slots with previos slots
        for(int d = 0; d < lTempSlots.Count; d++) {
            for(int e = d+1; e < lTempSlots.Count; e++) {
                if(lTempSlots[d].transform.position == lTempSlots[e].transform.position) {
                    lTempSlots[e].GetComponent<Slot>().enabled = false; lTempSlots[e].gameObject.SetActive(false);
                }
            }
        }    

    }

    private int counter = 0;
    private void showPosibleLocationsPortrait() {
          for(int i = 0; i < lSlots.Count; i++) {
                if(lSlots[i].GetComponent<Slot>().Orientation == EnumDominoOrientation.Portrait) {
                    for(int a = 0; a < 6; a++) {
                        GameObject go = (GameObject)Instantiate(slotPrefab, transform, false);
                        go.name = "new Slot";
                        go.GetComponent<Slot>().Location = EnumDominoLocations.Board;
                        go.GetComponent<Slot>().Orientation = EnumDominoOrientation.Portrait;
                        go.GetComponent<RectTransform>().localPosition = nextPositionFromSlotInBoardPortraitWithDominoPortrait(lSlots[i].transform, a);                        
                        if(hideSlots)go.GetComponent<Image>().color = new Color(0,0,0,0);
                        lTempSlots.Add(go);  
                    }
                }else if(lSlots[i].GetComponent<Slot>().Orientation == EnumDominoOrientation.Landscape) {
                    for(int a = 0; a < 8; a++) {      
                        GameObject go = (GameObject)Instantiate(slotPrefab, transform, false);
                        go.name = "new Slot";
                        go.GetComponent<Slot>().Location = EnumDominoLocations.Board;
                        go.GetComponent<Slot>().Orientation = EnumDominoOrientation.Portrait;
                        go.GetComponent<RectTransform>().localPosition = nextPositionFromSlotInBoardLandscapeWithDominoPortrait(lSlots[i].transform, a);
                        if(hideSlots)go.GetComponent<Image>().color = new Color(0,0,0,0);                                              
                        lTempSlots.Add(go);
                    } 
                }
                
            }
            //hide slots over other things(slots or dominoes)
            //hide slots with dominos
            for(int b = 0; b < lSlots.Count; b++) {
                for(int c = 0; c < lTempSlots.Count; c++) {
                    if(lSlots[b].transform.position == lTempSlots[c].transform.position) {
                        lTempSlots[c].GetComponent<Slot>().enabled = false; lTempSlots[c].gameObject.SetActive(false);
                    }
                }
            }
            //hide slots with previos slots
            for(int d = 0; d < lTempSlots.Count; d++) {
                for(int e = d+1; e < lTempSlots.Count; e++) {
                    if(lTempSlots[d].transform.position == lTempSlots[e].transform.position) {
                        lTempSlots[e].GetComponent<Slot>().enabled = false; lTempSlots[e].gameObject.SetActive(false);
                    }
                }

            }    
    }

    //OnEndDragOnTable also called when domino is left in slot in table
    private void checkIfNewSlotIsFilled() {
        if(isFirstTurn()) {
            if(lTempSlots.Count == 0)
                return;
            if(lTempSlots[0].transform.childCount > 0) {
                //filled
                lSlots.Add(lTempSlots[0]);
                lTempSlots[0].transform.GetChild(0).GetComponent<DominoEntity>().TableParent.GetComponent<Slot>().SwapOrientationToPortrait();
                lTempSlots[0].transform.GetChild(0).GetComponent<DominoEntity>().TableParent.GetComponent<Slot>().Show();
            } else {
                //not filled
                GameObject.Destroy(lTempSlots[0]);
            }
            lTempSlots.Clear();
        }else {
            bool hasMatch = false;
            for(int i = 0; i < lTempSlots.Count; i++) {
                //check if a temp slot was filled
                if(lTempSlots[i].transform.childCount > 0) {
                    for(int a = 0; a < lSlots.Count; a++) {
                        //check if has a match
                        if(lTempSlots[i].transform.GetChild(0).GetComponent<DominoEntity>().MatchWith(lSlots[a].transform.GetChild(0).GetComponent<DominoEntity>())){
                            //filled
                            hasMatch = true;                 
                            lSlots.Add(lTempSlots[i]);                            
                            lTempSlots[i].transform.GetChild(0).GetComponent<DominoEntity>().TableParent.GetComponent<Slot>().Show();
                            DominosValuesConfig.DoneCorrectMove();
                            //win est� en EasyCommandDominos
                            /*if(lSlots.Count == 10) {
                                EasyCommandDominos.Win();
                            }*/
                            break;
                        }               
                    }
                    if(hasMatch==false) {
						bool value;
						DominosValuesConfig.DoneIncorrectMove(out value);
						if (value == true)
							helpMessage.DisplayMessage ();
							
                        lTempSlots[i].transform.GetChild(0).GetComponent<DominoEntity>().BackToTable();                        
                    }  else {
                        break;
                    }              
                }              
            }
            //remove tempSlotsSobrantes
            for(int i = 0; i < lTempSlots.Count; i++) {
                if(lTempSlots[i].transform.childCount ==0) {
                    GameObject.Destroy(lTempSlots[i]);
                }
            }
            lTempSlots.Clear();

            //set locked elements
            for(int i = 0; i < lSlots.Count; i++) {                
                lSlots[i].transform.GetChild(0).GetComponent<DominoEntity>().Locked = true;
            }
        }

       
    }

    //when drop in table
    private void checkIfSlotsAreEmpty() {
        for(int i = 0; i < lSlots.Count; i++) {
            if(lSlots[i].transform.childCount == 0) {
                GameObject.Destroy(lSlots[i]);
                lSlots.RemoveAt(i);
            }
        }
    }


    private Vector2 nextPositionFromSlotInBoardPortraitWithDominoPortrait(Transform tr, int index) {
        switch(index) {
                case 0:
                    //Debug.Log(new Vector2(tr.localPosition.x - factor*2,    tr.localPosition.y + factor*2));
                return new Vector2(tr.localPosition.x - factor*2,    tr.localPosition.y + factor*2);
                case 1:
                return new Vector2(tr.localPosition.x,               tr.localPosition.y + factor*4);
                case 2:
                return new Vector2(tr.localPosition.x + factor*2,    tr.localPosition.y + factor*2);
                case 3:
                return new Vector2(tr.localPosition.x + factor*2,    tr.localPosition.y - factor*2);
                case 4:
                return new Vector2(tr.localPosition.x,               tr.localPosition.y - factor*4);
                case 5:
                return new Vector2(tr.localPosition.x - factor*2,    tr.localPosition.y - factor*2);
            }
        return Vector2.zero;
        
    }       
    private Vector3 nextPositionFromSlotInBoardPortraitWithDominoLanscape(Transform tr, int index) {
       switch(index) {
            case 0:
            return new Vector2(tr.localPosition.x - factor*3, tr.localPosition.y + factor);
            case 1:
            return new Vector2(tr.localPosition.x - factor, tr.localPosition.y + factor*3);
            case 2:
            return new Vector2(tr.localPosition.x + factor, tr.localPosition.y + factor*3);
            case 3:
            return new Vector2(tr.localPosition.x + factor*3, tr.localPosition.y + factor);
            case 4:
            return new Vector2(tr.localPosition.x + factor*3, tr.localPosition.y - factor);
            case 5:
            return new Vector2(tr.localPosition.x + factor, tr.localPosition.y-factor*3);
            case 6:
            return new Vector2(tr.localPosition.x -factor, tr.localPosition.y-factor*3);
            case 7:
            return new Vector2(tr.localPosition.x -factor*3, tr.localPosition.y-factor);
        }
        return Vector2.zero;
    }

    private Vector2  nextPositionFromSlotInBoardLandscapeWithDominoLanscape(Transform tr, int index) {
         switch(index) {
            case 0:
            return new Vector2(tr.localPosition.x - factor*4, tr.localPosition.y);
            case 1:
            return new Vector2(tr.localPosition.x-factor*2, tr.localPosition.y + factor*2);
            case 2:
            return new Vector2(tr.localPosition.x + factor*2, tr.localPosition.y + factor*2);
            case 3:
            return new Vector2(tr.localPosition.x + factor*4, tr.localPosition.y);
            case 4:
            return new Vector2(tr.localPosition.x+factor*2, tr.localPosition.y -factor*2);
            case 5:
            return new Vector2(tr.localPosition.x -factor*2, tr.localPosition.y-factor*2);
        }
        return Vector2.zero;
        
    }       

    private Vector3 nextPositionFromSlotInBoardLandscapeWithDominoPortrait(Transform tr, int index) {
       switch(index) {
            case 0:
            return new Vector2(tr.localPosition.x - factor*3, tr.localPosition.y + factor);
            case 1:
            return new Vector2(tr.localPosition.x - factor, tr.localPosition.y + factor*3);
            case 2:
            return new Vector2(tr.localPosition.x + factor, tr.localPosition.y + factor*3);
            case 3:
            return new Vector2(tr.localPosition.x + factor*3, tr.localPosition.y + factor);
            case 4:
            return new Vector2(tr.localPosition.x + factor*3, tr.localPosition.y -factor);
            case 5:
            return new Vector2(tr.localPosition.x +factor, tr.localPosition.y-factor*3);
            case 6:
            return new Vector2(tr.localPosition.x -factor, tr.localPosition.y-factor*3);
            case 7:
            return new Vector2(tr.localPosition.x -factor*3, tr.localPosition.y-factor);
        }
        return Vector2.zero;
    }
    /*
    private Vector2 nextPositionFromSlotInBoardPortraitWithDominoPortrait(Transform tr, int index) {
        switch(index) {
                case 0:
                return new Vector2(tr.localPosition.x - 100, tr.localPosition.y + 100);
                case 1:
                return new Vector2(tr.localPosition.x, tr.localPosition.y + 200);
                case 2:
                return new Vector2(tr.localPosition.x + 100, tr.localPosition.y + 100);
                case 3:
                return new Vector2(tr.localPosition.x + 100, tr.localPosition.y - 100);
                case 4:
                return new Vector2(tr.localPosition.x, tr.localPosition.y - 200);
                case 5:
                return new Vector2(tr.localPosition.x - 100, tr.localPosition.y - 100);
            }
        return Vector2.zero;
        
    }       
    private Vector3 nextPositionFromSlotInBoardPortraitWithDominoLanscape(Transform tr, int index) {
       switch(index) {
            case 0:
            return new Vector2(tr.localPosition.x - 150, tr.localPosition.y + 50);
            case 1:
            return new Vector2(tr.localPosition.x - 50, tr.localPosition.y + 150);
            case 2:
            return new Vector2(tr.localPosition.x + 50, tr.localPosition.y + 150);
            case 3:
            return new Vector2(tr.localPosition.x + 150, tr.localPosition.y + 50);
            case 4:
            return new Vector2(tr.localPosition.x + 150, tr.localPosition.y - 50);
            case 5:
            return new Vector2(tr.localPosition.x + 50, tr.localPosition.y-150);
            case 6:
            return new Vector2(tr.localPosition.x -50, tr.localPosition.y-150);
            case 7:
            return new Vector2(tr.localPosition.x -150, tr.localPosition.y-50);
        }
        return Vector2.zero;
    }

    private Vector2  nextPositionFromSlotInBoardLandscapeWithDominoLanscape(Transform tr, int index) {
         switch(index) {
            case 0:
            return new Vector2(tr.localPosition.x - 200, tr.localPosition.y);
            case 1:
            return new Vector2(tr.localPosition.x-100, tr.localPosition.y + 100);
            case 2:
            return new Vector2(tr.localPosition.x + 100, tr.localPosition.y + 100);
            case 3:
            return new Vector2(tr.localPosition.x + 200, tr.localPosition.y);
            case 4:
            return new Vector2(tr.localPosition.x+100, tr.localPosition.y -100);
            case 5:
            return new Vector2(tr.localPosition.x -100, tr.localPosition.y-100);
        }
        return Vector2.zero;
        
    }       

    private Vector3 nextPositionFromSlotInBoardLandscapeWithDominoPortrait(Transform tr, int index) {
       switch(index) {
            case 0:
            return new Vector2(tr.localPosition.x - 150, tr.localPosition.y + 50);
            case 1:
            return new Vector2(tr.localPosition.x - 50, tr.localPosition.y + 150);
            case 2:
            return new Vector2(tr.localPosition.x + 50, tr.localPosition.y + 150);
            case 3:
            return new Vector2(tr.localPosition.x + 150, tr.localPosition.y + 50);
            case 4:
            return new Vector2(tr.localPosition.x + 150, tr.localPosition.y -50);
            case 5:
            return new Vector2(tr.localPosition.x +50, tr.localPosition.y-150);
            case 6:
            return new Vector2(tr.localPosition.x -50, tr.localPosition.y-150);
            case 7:
            return new Vector2(tr.localPosition.x -150, tr.localPosition.y-50);
        }
        return Vector2.zero;
    }
    */
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FilePanelDisplay : MonoBehaviour {
	private Animator anim;
	private bool isShowing;
	private GameObject currentFile;
	[SerializeField] private UnityEvent onShowing;
	[SerializeField] private UnityEvent onHiding;

	void Awake(){
		anim = GetComponent<Animator> ();
	}

	public void ListenerShowCurrentFile(GameObject prefab){
		GameObject go = (GameObject)Instantiate(prefab, transform, false);
		GameObject.Destroy (currentFile);
		currentFile = go;

	}

	public void ListenerShow(){
		onShowing.Invoke ();
		anim.Play ("anim_file_intro");
		isShowing = true;
	}

	public void ListenerHide(){
		onHiding.Invoke ();
		anim.Play ("anim_file_out");
		isShowing = false;
	}

	public void ListenerSwapShowHide(){ 
		///if(anim.GetCurrentAnimatorStateInfo(0).IsName("anim_file_intro")){
		if(anim.GetCurrentAnimatorStateInfo(0).normalizedTime <1){
			return;
		}
		if(isShowing){
			ListenerHide ();
		}else{ListenerShow();}
	}


}

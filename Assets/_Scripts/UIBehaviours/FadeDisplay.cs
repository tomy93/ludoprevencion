using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeDisplay : MonoBehaviour {
    public static FadeDisplay Instance;

    void Awake() {
        Instance = this;
    }
    
    public void Show() {
        gameObject.SetActive(true);
    }   

    //llamado cuando deber�a ejecutarse el cambio de escena
    public void ListenerFadeOutStart() {
        SceneSwitch.GotoNextScene();
    }

}

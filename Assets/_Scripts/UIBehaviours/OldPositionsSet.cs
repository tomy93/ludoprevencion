﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OldPositionsSet : MonoBehaviour {
    [SerializeField] private Slot[] aSlots;
    [SerializeField] private DominoEntity[] aDominos;

    void Awake() {
        if(DominosValues.FirstTimePlaying == false) {
           setOldPositions();
        }
    }
    //step1
    private void setOldPositions() {
        for(int i = 0; i < aDominos.Length; i++) {
            aDominos[i].transform.SetParent(getDominoParent(aDominos[i].dominoID));
            aDominos[i].transform.localPosition = Vector3.zero;
        }
    }
    //step2
    private Transform getDominoParent(int dominoID) {
        for(int i = 0; i < DominosValues.aDomPosVec.Length; i++) {
            if(DominosValues.aDomPosVec[i].DominoID == dominoID) {
                return getParentSlot(DominosValues.aDomPosVec[i]);                
            }
        }
        return null;
    }
    //step3
    private Transform getParentSlot(DominoPositionVector dominoPositionVector) {
        for(int i = 0; i < aSlots.Length; i++) {
            if(aSlots[i].SlotID == dominoPositionVector.SlotID) {
                return aSlots[i].transform;
            }
        }
        return null;
    }   
    void Start() {

    }
	
}

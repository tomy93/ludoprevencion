using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValidateAnswers : MonoBehaviour {
    [SerializeField] private CurrentCaseInit caseInit;
    [SerializeField] private CaseAnswerValidation[] aCaseAnswersValidation;
    [SerializeField] private float timeShowingValidations = 2.0f;
    [SerializeField] private GameObject filter;
    private bool lastValidation= false;
    private bool isValidating = false;
    private bool gameEnded = false;

    public void ListenerValidateAnswers() { 
        if(gameEnded)return;
        if(!canValidate())return;
        if(CaseGameValues.MAX_AMOUNT_VALIDATIONS - CaseGameValues.CurrentValidations == 1) {
            lastValidation = true;
        }
        validate(); 
    }

    private void validate() {
        isValidating = true;
        filter.SetActive(true);
        int countCorrect = 0;
        int countIncorrect = 0;
        Invoke("endValidation", timeShowingValidations);  
		CaseAnswer[] aCaseAnswer = null;
		switch (caseInit.CaseUsed) {
			case 0:
				aCaseAnswer = caseInit.aCase1CorrectAnswers;
				break;
			case 1:
				aCaseAnswer = caseInit.aCase2CorrectAnswers;
				break;
			case 2:
				aCaseAnswer = caseInit.aCase3CorrectAnswers;
				break;
			default:
				break;
		}

        for(int i = 0; i < aCaseAnswersValidation.Length; i++) {
            if(aCaseAnswersValidation[i].checkBox.isMarked) {
                bool isCorrect=false;
				for(int f = 0; f < aCaseAnswer.Length; f++) {
					if(aCaseAnswersValidation[i].caseType == aCaseAnswer[f].caseType
						&& aCaseAnswersValidation[i].checkBoxIndex == aCaseAnswer[f].index
                        ) {
                        aCaseAnswersValidation[i].SetCorrect();
                        isCorrect = true;
                        countCorrect++;
                        break;
                    }
                }
                if(!isCorrect) {
                    aCaseAnswersValidation[i].SetIncorrect();
                    countIncorrect++;
                }
            }
        }
		Debug.Log (countCorrect +"___" +countIncorrect );
		if(countCorrect == aCaseAnswer.Length && countIncorrect == 0) {
            lastValidation = true;
        }   

        if(lastValidation) {
            for(int i = 0; i < aCaseAnswersValidation.Length; i++) {
                if(aCaseAnswersValidation[i].isCorrect) {
                    aCaseAnswersValidation[i].ShowCorrectCheck();
                }
            }
        }
    }

    private bool canValidate() {
        bool canValidate = false;
        for(int i = 0; i < aCaseAnswersValidation.Length; i++) {
            if(aCaseAnswersValidation[i].checkBox.isMarked) {
                canValidate = true;
                break;
            }
        }
        return canValidate;
    }

    //validation when clock is over
    public void ListenerLastValidation() {
        if(gameEnded)return;
        //if(isValidating)return;
        lastValidation = true;
        validate();
    }

    private void endValidation() {
        for(int i = 0; i < aCaseAnswersValidation.Length; i++) {
            if(aCaseAnswersValidation[i].checkBox.isMarked) {
                aCaseAnswersValidation[i].ListenerHideValidation();
            }
        }
        filter.SetActive(false);
        isValidating = false;
        if(lastValidation) {
            gameEnded = true;
            CaseGameValuesConfig.EndGame();
        }else {
            CaseGameValuesConfig.CountValidation();
        }
    }

	void Start () {		
        filter.SetActive(false);
	}
	
	
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TimeDisplay : MonoBehaviour {
    [SerializeField] private Text txtTime;
    private float currentTime;
    private bool canCount = true;
    [Space(10)]
    [SerializeField]private UnityEvent OnEndTime;

	void Start () {
		currentTime = CaseGameValues.TOTAL_TIME;
	}
	
	void LateUpdate () {
        if(!canCount)return;
		currentTime -=Time.deltaTime;
        txtTime.text = "Tiempo : "+ Mathf.Ceil(currentTime);
        CaseGameValuesConfig.SetCurrentTimeLeft((int)Mathf.Ceil(currentTime));
        if(Mathf.Ceil(currentTime) == 0) {
            Debug.Log("acab� tiempo");
            OnEndTime.Invoke();
            canCount = false;
        }
	}

    public void ListenerStopTimer() {
        canCount = false;
    }
}

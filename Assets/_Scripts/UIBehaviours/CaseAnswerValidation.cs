using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CheckBoxDisplay))]
public class CaseAnswerValidation : MonoBehaviour {
    [SerializeField] public EnumCaseType caseType;
	[SerializeField] public int checkBoxIndex;
    [HideInInspector] public CheckBoxDisplay checkBox;
    [SerializeField] private Image imgCorrect;
    [SerializeField] private Sprite spCorrect;
    [SerializeField] private Sprite spIncorrect;

    public bool isCorrect;   

    public void SetCorrect() {
        imgCorrect.sprite = spCorrect;
        imgCorrect.enabled = false;
        //así evitamos que consiga doble puntos la siguiente vez que valide
        if(!isCorrect) {
            CaseGameValuesConfig.DoneCorrectMove();
        }
        isCorrect = true;
    }

    public void ShowCorrectCheck() {
        imgCorrect.enabled = true;
    }
    public void SetIncorrect() {
        imgCorrect.sprite = spIncorrect;
        imgCorrect.enabled = true;
        CaseGameValuesConfig.DoneIncorrectMove();
    }

    public void ListenerCheckBoxSwap() {
        isCorrect = false;
    }

    public void ListenerHideValidation() {
        imgCorrect.enabled = false;
    }

	void Awake () {
		checkBox = GetComponent<CheckBoxDisplay>();
	}
	

}

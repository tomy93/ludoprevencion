using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CheckBoxDisplay : MonoBehaviour, IPointerClickHandler {	
	public bool isMarked;
	[SerializeField] private Image img;
	[SerializeField] private Sprite spLocked;

    private void Awake() {
       
    }

	private void swapLocked(){
        if(isMarked) {            
		    isMarked = false;
            img.enabled = false;
        }else {
		    isMarked = true;
            img.enabled = true;
            img.sprite = spLocked;
        }
	}

    public void OnPointerClick(PointerEventData eventData) {
        swapLocked();
    }
}

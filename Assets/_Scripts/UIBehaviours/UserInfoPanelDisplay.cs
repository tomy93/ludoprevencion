using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInfoPanelDisplay : MonoBehaviour {
    [SerializeField]private BasicPanelDisplay panelDisplay;

    void Awake() {       
    }
    void OnEnable() {    
        //esto muestra el panel si vienes del último juego
        if(SceneSwitch.lastScene == ScenesNamesLibrary.ACCIDENT_CASE) {
            panelDisplay.ListenerSwapActive();
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Animator))]
public class MinigameAnimEndDisplay : MonoBehaviour {
    //[SerializeField] private float timeToChangeScene=3;
    [SerializeField] private EnumMinigame minigame;
    private Transform child;
    private enum EnumMinigame {None, Dominos, Solitary };
    private Animator animator;

    void Awake() {
        child = transform.GetChild(0);
        child.gameObject.SetActive(false);
        animator = GetComponent<Animator>();        
    }

	void Start () {
		//OnEnable();
	}

    void OnEnable() {
        child.gameObject.SetActive(false);

        if(minigame == EnumMinigame.Dominos) {
            DominosActions.OnGameEnd += showAnimation;
        }else if(minigame == EnumMinigame.Solitary){
            SolitaryActions.OnGameEnd += showAnimation;
        }
    }
    void OnDisable() {
        if(minigame == EnumMinigame.Dominos) {
            DominosActions.OnGameEnd -= showAnimation;
        }else if(minigame == EnumMinigame.Solitary){
            SolitaryActions.OnGameEnd -= showAnimation;
        }
    }  

    private void showAnimation() {
        child.gameObject.SetActive(true);
        //Invoke("endGame",timeToChangeScene);
        animator.Play("minigameEnd_intro");
    }

    public void ListenerEndGame() {
        child.gameObject.SetActive(false);
         if(minigame == EnumMinigame.Dominos) {
           SceneSwitch.GotoSolitary();
        }else if(minigame == EnumMinigame.Solitary){
           SceneSwitch.GotoAccidentCase();
        }
    }

}

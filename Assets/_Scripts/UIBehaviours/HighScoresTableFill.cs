using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScoresTableFill : MonoBehaviour {
    [SerializeField] private GameObject prefabRow;
    [SerializeField] private GameObject panelHighScores;
    [SerializeField] private GameScenesReload scenesReload;

    private void Start() {
        
    }

    private void OnEnable() {
        HighScoresActions.OnInfoFill+=addCurrentUserRow;

        Debug.Log("***Request here users to database");
        addRow(new User(0,"aa0","pelu", 100));
        addRow(new User(0,"aa3","pelu", 500));
        addRow(new User(0,"aa1","pelu", 100));
        addRow(new User(0,"aa3","pelu", 700));
        addRow(new User(0,"aa3","pelu", 800));
        addRow(new User(0,"aa2","pelu", 200));
        addRow(new User(0,"aa3","pelu", 300));

        orderRows();
    }

    private void OnDisable() {
        HighScoresActions.OnInfoFill-=addCurrentUserRow;
        ListenerDestroyRows();
    }    

    private void addCurrentUserRow() {
        addRow(GameValues.CurrentUser);
        Debug.Log("***Send here User info to Database");
        orderRows();

        GameValuesConfig.ResetGame();
        scenesReload.UnloadGameScenes();
    }

    private void addRow(User user) {
        GameObject go = Instantiate(prefabRow, panelHighScores.transform);
        go.GetComponent<HighScoreRowDisplay>().SetInfo(user.Name,user.Country, user.Score);
        go.transform.localScale = Vector3.one;
    }
    
    public void ListenerDestroyRows() {
        for(int i = 0; i < panelHighScores.transform.childCount; i++) {
            GameObject.Destroy(panelHighScores.transform.GetChild(i).gameObject);
        }
    }

    private void orderRows() {
        List< HighScoreRowDisplay> lRow = new List<HighScoreRowDisplay>();
        foreach(Transform item in panelHighScores.transform) {
            lRow.Add(item.GetComponent<HighScoreRowDisplay>());
        }
        for(int f = 0; f < lRow.Count-1; f++) {
            for(int i = 1; i < lRow.Count; i++) {
                if(lRow[f].Score < lRow[i].Score) {
                    lRow[f].transform.SetSiblingIndex(lRow[i].transform.GetSiblingIndex());
                }

            }
        }

    }

}
